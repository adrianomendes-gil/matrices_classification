function [subj_files grp obs z_scores] = neural_measures(dataset_path)
% NEURAL_MEASURES 

if nargin == 0
    dataset_path = '../../../datasets/freelancer/alzheimer_brain_measures/Results/';
end;

files = dir(dataset_path);
n_files = length(files);
n_samples = n_files-2;

feature_size = 200*200;

subj_files = cell(1,n_samples);

grp = cell(n_samples,1);
obs = zeros(n_samples, feature_size);

%% Loading files
for i = 3:n_files
    load([dataset_path files(i).name]);

    subj_data.blah = blah;
    subj_data.centre = centre;
    subj_data.corr_matrix = corr_matrix;
    subj_data.fisher_matrix = fisher_matrix;
    subj_data.subj = subj;
    subj_data.feature_vector = fisher_matrix(:);

    subj_files{1,i-2} = subj_data;

    grp{i-2, 1} = centre;
    obs(i-2, :) = fisher_matrix(:)'; 
end;

n_test = ceil(0.2*n_samples);
n_train = n_samples - n_test;

holdout_CVP = cvpartition(grp, 'holdout', n_test);
data_train = obs(holdout_CVP.training,:);
grp_train = grp(holdout_CVP.training);

z_scores = get_fs_by_thresholding(data_train, grp_train, 0.2);

end

function z_scores = get_fs_by_thresholding(x_train, y_train, t)
% Get scores regarding feature selection using a threshold t 

[n_samples feature_size] = size(x_train);

z_scores = zeros(feature_size,1);

for x = 1:feature_size
    for y1 = 1:max(grp2idx(y_train))
        y1_idx = grp2idx(y_train) == y1;
        for y2 = (y1+1):max(grp2idx(y_train))
            y2_idx = grp2idx(y_train) == y2;
            z_scores(x) = z_scores(x) + abs(mean(x_train(y1_idx, x)) - mean(x_train(y2_idx, x)));
        end;
    end;
    z_scores(x) = abs(z_scores(x));
end;

end